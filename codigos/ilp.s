// assume r0=x, r1=y, r2=z
mul r0, r0, r0
mul r1, r1, r1
mul r2, r2, r2
add r0, r0, r1
add r3, r0, r2
// now r3 stores value of program variable 'a'
