typedef struct {
	int N;
	int terms;
	float* x;
	float* result;
} my_args;

void parallel_sinx(int N, int terms, float* x, float* result)
{
	pthread_t thread_id;
	my_args args;

	args.N = N/2;
	args.terms = terms;
	args.x = x;
	args.result = result;

	pthread_create(&thread_id, NULL, my_thread_start, &args); // launch thread result[i] = value;
	sinx(N - args.N, terms, x + args.N, result + args.N); // do work
	pthread_join(thread_id, NULL);
}

void my_thread_start(void* thread_arg)
{
	my_args* thread_args = (my_args*)thread_arg;
	sinx(args->N, args->terms, args->x, args->result); // do work
}
