\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}
\usepackage{multimedia}
\usepackage{copyrightbox}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Teoria do desempenho em paralelismo}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Universidade Presbiteriana Mackenzie}

\date{\today} % Date, can be changed to a custom date
% \logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}



\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Desempenho e escalabilidade}


\begin{frame}
    \frametitle{O que é desempenho?}
    \begin{itemize}
        \item Desempenho em computação pode ser definido através dois fatores \cite{oregoncis410}:
        \begin{enumerate}
            \item Requisitos computacionais (o que precisa ser feito);
            \item Recursos computacionais (o que custa pra fazer).
        \end{enumerate}
        \item Problemas computacionais se traduzem em requisitos;
        \item O desempenho computacional é diretamente proporcional à quantidade de recursos necessários para resolver o problema.
    \end{itemize}
    \begin{figure}[ht!]
         \centering
         \includegraphics[width=.8\textwidth]{../imagens/performance-resource}
         \label{fig:performance-resource}
    \end{figure}
\end{frame}


\begin{frame}
    \frametitle{Por que o desempenho é importante?}
    \begin{itemize}
        \item O desempenho é uma medida que avalia se os recursos computacionais \alert{disponíveis} estão sendo utilizados em sua \alert{totalidade};
        \item A avaliação de desempenho é importante para entender o relacionamento entre requisitos e recursos disponíveis:
        \item Auxilia no desenho da solução para atingir os objetivos;
        \item As medidas de desempenho refletem as decisões de \alert{como} e \alert{com qual eficiência} as soluções propostas são capazes de satisfazer os requisitos computacionais.
    \end{itemize}
    \begin{quote}
        ``A dificuldade mais comum no processo de concepção do motor veio do desejo em reduzir o tempo que os cálculos eram executados para o mínimo possível.'' \\
        Charles Babbage, 1791 – 1871
    \end{quote}
\end{frame}

\begin{frame}
    \frametitle{O que é desempenho em paralelismo?}
    \begin{itemize}
        \item O desempenho no contexto do paralelismo trata de resolver os problemas encontrados ao utilizar um ambiente computacional paralelo;
        \item Está diretamente relacionado ao desempenho em \alert{computação} paralela;
        \item O desempenho é a razão de existência do paralelismo:
        \begin{itemize}
            \item Performance paralela versus sequencial;
            \item Se o desempenho não for melhor o paralelismo não é necessário;
        \end{itemize}
        \item Processamento paralelo inclui as técnicas e tecnologias necessárias para computar em paralelo (hardware, rede, bibliotecas, linguagens de programação, etc);
        \item O paralelismo deve entregar \alert{ganho de desempenho} de forma \alert{objetiva} (Quanto? Como?)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Expectativa de desempenho (perda)}
    \begin{itemize}
        \item Se cada processador possui um desempenho de $k$ MFLOPS e eu tenho $p$ processadores, o desempenho deve ser $k \times p$?
        \item Se leva 100 segundos para rodar em um processador, em 10 processadores deveria levar 10 segundos?
        \item Existem muitas causas que afetam a performance:
        \begin{itemize}
            \item Cada uma das causas deve ser analisada separadamente;
            \item Mesmo assim podem influenciar umas nas outras de formas muito complexas:
            \begin{itemize}
                \item A solução para um problema pode criar outro;
                \item Um problema pode mascarar a ocorrência de outro.
            \end{itemize}
        \end{itemize}
        \item Escalar o problema (tamanho, sistema) pode mudar as condições;
        \item É necessário entender o ``espaço de desempenho''.
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Paralelismo computacional embaraçoso}
    \begin{itemize}
        \item \alert{Paralelismo embaraçoso}: o programa pode ser dividido em partes completamente independentes que podem ser executadas de forma simultânea.
        \begin{itemize}
            \item Em um ambiente paralelo verdadeiramente embaraçoso não há nenhuma interação entre os processos separados;
            \item Em um ambiente paralelo quase embaraçoso os resultados da computação podem ser coletados/combinados de alguma forma.
        \end{itemize}
        \item Computações paralelas embaraçosas têm potencial para obter \emph{speedup} máximo em plataformas paralelas.
        \begin{itemize}
            \item Se o programa executa de forma sequencial no tempo $T$ ao executar em $P$ processadores o tempo potencial é possível obter tempo $\frac{T}{P}$;
            \item Por que nem sempre esse potencial é atingido?
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Escalabilidade}
    \begin{itemize}
        \item O programa pode escalar até usar muitos processadores. O que isso significa?
        \item Como avaliar escalabilidade?
        \item Como avaliar a qualidade da escalabilidade?
        \item Avaliação comparativa:
        \begin{itemize}
            \item Se dobrar o número de processadores, o que é possível esperar?
            \item A escalabilidade é linear?
        \end{itemize}
        \item Utilizar uma medida de \alert{eficiência} paralela: ela é mantida conforme o problema aumenta de tamanho?
        \item Aplicar métricas de desempenho.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Desempenho e escalabilidade}
    \begin{itemize}
        \item Avaliação:
        \begin{itemize}
            \item O tempo de execução \alert{sequencial} $T_{seq}$ é uma função do tamanho do problema e da arquitetura;
            \item O tempo de execução \alert{paralelo} $T_{par}$ é uma função do:
            \begin{itemize}
                \item tamanho do problema;
                \item número de processadores utilizados na execução.
            \end{itemize}
        \end{itemize}
        \item Escalabilidade: capacidade que um algoritmo paralelo tem de obter ganho de performance proporcional à quantidade de processadores e ao tamanho do problema.
    \end{itemize}
\end{frame}

\section{Métricas de desempenho}

\begin{frame}
    \frametitle{Métricas e fórmulas de desempenho}
    \begin{itemize}
        \item $T_1$ é o tempo de execução em um único processador;
        \item $T_p$ é o tempo de execução em um sistema com $p$ processadores;
        \item $S(p)$ ($S_p$) é o \textit{speedup}:
        \[
            S_p = \frac{T_1}{T_p}
        \]
        \item $E(p)$ ($E_p$) é a eficiência:
        \[
            E_p = \frac{S_p}{p}
        \]
        \item $Cost(p)$ ($C_p$) é o custo:
        \[
            C_p = p \times T_p
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Speedup e eficiência}
    \begin{itemize}
        \item É possível observar que a eficiência representa  evolução do speedup de acordo com a quantidade de processadores;
        \item Em geral existe um custo representado que \alert{reduz} a eficiência de adicionar mais processadores. 
    \end{itemize}
    \begin{table}[ht!]
        \centering
        \includegraphics[width=.6\textwidth]{../imagens/speedup-efficiency}
        \label{fig:speedup-efficiency}    
        \caption{Relação entre eficiência e speedup para um programa paralelo \cite{pacheco2021introduction}}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Speedup linear}
    \begin{itemize}
        \item O algoritmo paralelo tem \alert{custo ótimo} quando:
        \[
            \text{tempo paralelo} = \text{tempo sequencial} \Rightarrow (C_p = T_1, E_p = 100\%)
        \]
        \item Nesse caso, podemos dizer que:
        \[
            \begin{split}
                (C_p = T_1,  C_p = p \times T_p) &\Rightarrow T_1 = p \times T_p \\
                &\Rightarrow T_p = \frac{T_1}{p}
            \end{split}
        \]
        \item Quando o custo é ótimo, podemos dizer que o \alert{speedup é linear}.
    \end{itemize}
\end{frame}

\subsection{Lei de Amdahl}


\begin{frame}
    \frametitle{Lei de Amdahl}
    \begin{itemize}
        \item Seja $f$ a fração do programa que é sequencial. Assim, $1-f$ é a fração que pode ser paralelizada;
        \item Seja $T_1$ o tempo de execução em um único processador;
        \item Seja $T_p$ o tempo de execução em $p$ processadores calculado em função da fração que pode ser paralelizada:
        \[
            T_p = f T_1 + (1-f)\frac{T_1}{p} =  T_1 \times \left(f + \frac{1-f}{p}\right)
        \]
        \item $S_p$ é o \textit{speedup}:
        \[
            \begin{split}
                S_p &= \frac{T_1}{T_p} \\
                S_p &= \frac{T_1}{T_1 \times \left(f + \frac{1-f}{p}\right)} = \frac{1}{f + \frac{1-f}{p}}
            \end{split}
        \]
        \item Quando $p \rightarrow \infty$:
        \[
            S_p = \frac{1}{f}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Lei de Amdahl -- interpretação}
    \begin{itemize}
        \item Podemos observar que a escalabilidade do programa depende da fração paralelizável;
        \item Adicionar mais processadores não significa, necessariamente, executar mais rápido.
    \end{itemize}
    \bigskip
    \begin{quote}
        A não ser que todo um programa serial possa ser paralelizado, o speedup possível também será limitado, independente do número de núcleos disponíveis. \cite{pacheco2021introduction}
    \end{quote}
\end{frame}

\begin{frame}
    \frametitle{Lei de Amdahl -- exemplo speedup}
    \begin{itemize}
        \item Suponha que seja possível paralelizar 90\% de um programa serial;
        \item A paralelização é ``perfeita'' independente da quantidade de núcleos $p$ utilizados;
        \item Vamos supor que o tempo serial $T_1 = 20 \text{ segundos}$;
        \item Comecemos pelo cálculo do speedup:
        \[
            \begin{split}
                S_p &= \frac{1}{f + \frac{1-f}{p}} \\
                  &= \frac{1}{0,1 + \frac{0,9}{p}} \\
                 S_p &= \frac{p}{0,1p + 0,9}
            \end{split}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Lei de Amdahl -- exemplo tempo paralelo}
    \begin{itemize}
        \item Agora vamos supor que o tempo serial $T_1 = 20 \text{ segundos}$;
        \item Vejamos o tempo de execução paralela:
        \[
            \begin{split}
                T_p &= T_1 \times \left(f + \frac{1-f}{p}\right) \\
                  &= 20 \times \left(0,1 + \frac{0,9}{p}\right) \\
                  T_p &= \frac{18}{p} + 2
            \end{split}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Lei de Amdahl -- gráfico}
    \begin{figure}[ht!]
         \centering
         \includegraphics[width=.9\textwidth]{../imagens/lei-amdahl}
         \label{fig:lei-amdahl}
         \caption{Representação da Lei de Amdahl e o \alert{fator de speedup}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lei de Amdahl e escalabilidade}
    \begin{itemize}
        \item \alert{Escalabilidade}: capacidade que o algoritmo paralelo tem de obter ganho de performance proporcional ao número de processadores e ao tamanho do problema.
        \item Quando a Lei de Amdahl se aplica?
        \begin{itemize}
            \item Quando o tamanho do problema é fixo;
            \item \alert{Escalabilidade forte}:
                \[
                    p \rightarrow \infty, S_p = S_\infty \rightarrow \frac{1}{f}
                \]
            \item O limite de \emph{speedup} é determinado pela fração do programa cuja execução é sequencial, e não pelo número de processadores!
            \item Por que isso não é bom?
            \pause
            \item \alert{Eficiência perfeita} é difícil de obter.
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Lei de Gustafson-Barsis}


\begin{frame}
    \frametitle{Lei de Gustafson-Barsis (speedup escalável)}
    \begin{itemize}
        \item Na maior parte dos casos está interessada em tratar escalabilidade de problemas maiores:
        \begin{itemize}
            \item Qual o tamanho do problema que pode ser executado;
            \item Restringe o tamanho do problema ao tempo de execução paralela.
        \end{itemize}
        \item Assuma que o tempo de execução paralela seja constante:
        \begin{itemize}
            \item $T_p = C = (f + (1-f)) \times C$
            \item $f_{seq}$ é a fração de $T_p$ gasta na execução sequencial;
            \item $f_{par}$ é a fração de $T_p$ gasta na execução paralela;
            \item Lembrando que $f_{par} = 1 - f_{seq}$.
        \end{itemize}
        \item Qual o tempo de execução em um processador?
        \[
            \text{Se } C = 1, \text{então } T_s = f_{seq} + p(1-f_{seq}) = 1 + (p-1)f_{par}
        \]
        \item Qual é o speedup no caso?
        \[
            S_p = \frac{T_s}{T_p} = \frac{T_s}{1} = f_{seq} + p(1 - f_{seq}) = 1 + (p-1)f_{par}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Lei de Gustafson-Barsis e escalabilidade}
    \begin{itemize}
        \item \alert{Escalabilidade}: capacidade que o algoritmo paralelo tem de obter ganho de performance proporcional ao número de processadores e ao tamanho do problema.
        \item Quando a Lei de Gustafson se aplica?
        \begin{itemize}
            \item Quando o tamanho do problema pode aumentar de acordo com o aumento do número de processadores;
            \item \alert{Escalabilidade fraca}: 
                \[
                    S_p = 1 + (p-1)f_{par}
                \]
            \item A função de speedup inclui o número de processadores;
            \item Pode manter ou aumentar a eficiência do paralelismo à medida que o problema escala.
        \end{itemize}
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Escalabilidade de Amdahl}
    \begin{figure}[ht!]
         \centering
         \includegraphics[width=1\textwidth]{../imagens/amdahl-scalability}
         \label{fig:amdahl-scalability}
         \caption{Escalabilidade na Lei de Amdahl \cite{oregoncis410}}
    \end{figure}
\end{frame}


\begin{frame}
    \frametitle{Escalabilidade de Gustafson-Barsis}
    \begin{figure}[ht!]
         \centering
         \includegraphics[width=1\textwidth]{../imagens/gustafson-scalability}
         \label{fig:gustafson-scalability}
         \caption{Escalabilidade na Lei de Gustafson-Barsis \cite{oregoncis410}}
    \end{figure}
\end{frame}

\section{Modelagem e estimativas}

\begin{frame}
    \frametitle{Modelo de grafos para a computação}
    \begin{minipage}[ht!]{0.79\textwidth}
	    \begin{itemize}
	        \item Vamos pensar em um programa paralelo como um Grafo Direcional Acíclico (\emph{Directed Acyclic Graph  -- DAG}):
	        \begin{itemize}
	            \item Uma tarefa não pode ser executada até que todas as entradas necessárias estejam disponíveis;
	            \item As entradas são saídas de tarefas executadas anteriormente;
	            \item O DAG mostra as dependências de forma explícita.
	        \end{itemize}
	        \item Pense no hardware como um conjunto de \emph{workers} (processadores);
	        \item Vamos considerar um escalonador \alert{guloso} que envie todas as tarefas do DAG para os \emph{workers}
	        \item Nenhum \emph{worker} estará parado enquanto houver tarefas para executar.
	    \end{itemize}
    \end{minipage}
    \begin{minipage}[ht!]{0.19\textwidth}
	    \begin{figure}[ht!]
	         \centering
	         \includegraphics[width=1\textwidth]{../imagens/dag-example}
	         \label{fig:dag-example}
	    \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Modelo de período de processamento (\emph{Work-span})}
    \begin{minipage}[ht!]{0.79\textwidth}
	    \begin{itemize}
	        \item $T_p$: tempo para executar $P$ \textit{workers};
	        \item $T_1$: processamento (\emph{work})
	        \begin{itemize}
	            \item Tempo para execução serial, ou seja, execução de todas as tarefas por um \emph{worker};
	            \item Soma de toda a computação realizada.
	        \end{itemize}
	        \item $T_\infty$: período (\emph{span})
	        \begin{itemize}
	            \item Tempo para percorrer o caminho crítico
	        \end{itemize}
	        \item Caminho crítico:
	        \begin{itemize}
	            \item Sequência de execução de tarefas ou caminho no grafo (DAG) que leva a maior quantidade de tempo para executar;
	            \item Assume que um número infinito de \emph{workers} está disponível.
	        \end{itemize}
	    \end{itemize}
    \end{minipage}
    \begin{minipage}[ht!]{0.19\textwidth}
	    \begin{figure}[ht!]
	         \centering
	         \includegraphics[width=1\textwidth]{../imagens/dag-example}
	         \label{fig:dag-example2}
	    \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Exemplo de período de processamento (\emph{Work-span})}
    \begin{minipage}[ht!]{0.79\textwidth}
	    \begin{itemize}
            \item Considere que cada tarefa leva 1 unidade de tempo para executar;
            \item A DAG da Figura possui 7 tarefas;
            \item $T_1 = 7$
            \begin{itemize}
                \item Todas as tarefas devem ser executadas;
                \item As tarefas são executadas em ordem;
                \item A execução pode ser realizada em qualquer ordem?
            \end{itemize}
            \item $T_\infty = 5$
            \begin{itemize}
                \item Tempo para percorrer o caminho crítico;
                \item No caso, é o maior caminho no grafo para qualquer tarefa finalizada que tenha executado todas as suas dependências.
            \end{itemize}
	    \end{itemize}
    \end{minipage}
    \begin{minipage}[ht!]{0.19\textwidth}
	    \begin{figure}[ht!]
	         \centering
	         \includegraphics[width=1\textwidth]{../imagens/dag-critical}
	         \label{fig:dag-critical}
	    \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Limites inferior e superior no escalonamento guloso}
    \begin{minipage}[ht!]{0.79\textwidth}
	    \begin{itemize}
            \item Suponha que tenhamos $P$ \emph{workers};
            \item É possível escrever uma fórmula no modelo \emph{work-span} para deduzir um limite inferior em $T_p$:
            \[
                Max \left(\frac{T_1}{P}, T_\infty \right) \leq T_p
            \]
            \item $T_\infty$ é o melhor tempo de execução possível;
            \item O Lemma de Brent deduz um limite superior:
            \begin{itemize}
                \item Calcule o custo adicional para executar as outras tarefas que não estão no caminho crítico;
                \item Assuma que é possível executar as tarefas sem sobrecarregar o sistema (\emph{overhead});
                \[
                    T_p \leq \frac{T_1 - T_\infty}{P} + T_\infty
                \]
            \end{itemize}
	    \end{itemize}
    \end{minipage}
    \begin{minipage}[ht!]{0.19\textwidth}
	    \begin{figure}[ht!]
	         \centering
	         \includegraphics[width=.8\textwidth]{../imagens/dag-lower-upper}
	         \label{fig:dag-lower-upper}
	    \end{figure}
    \end{minipage}
\end{frame}


\begin{frame}
    \frametitle{Lemma de Brent para dois processadores}
    \begin{minipage}[ht!]{0.79\textwidth}
	    \begin{itemize}
            \item $T_1 = 7$
            \item $T_\infty = 5$
            \item Calculando $T_2$ (2 processadores):
            \[
                \begin{split}
                    T_2 &\leq \frac{T_1 - T_\infty}{P} + T_\infty \\
                        &\leq \frac{7-5}{2} + 5 \\
                        &\leq 6
                \end{split}
            \]
	    \end{itemize}
    \end{minipage}
    \begin{minipage}[ht!]{0.19\textwidth}
	    \begin{figure}[ht!]
	         \centering
	         \includegraphics[width=1\textwidth]{../imagens/dag-brent}
	         \label{fig:dag-brent}
	    \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Amdahl era muito otimista!}
    \begin{figure}[ht!]
         \centering
         \includegraphics[width=.85\textwidth]{../imagens/dag-comparison}
         \label{fig:dag-comparison}
         \caption{Gráfico comparativo para o speedup com múltiplos processadores \cite{oregoncis410}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Estimando tempo de execução}
    \begin{itemize}
        \item Escalabilidade requer que o cálculo de $T_\infty$ seja dominado por $T_1$
        \[
            T_p \approx \frac{T_1}{P} \text{ se } T_\infty < < T_1
        \]
        \item Aumentar o processamento ($T_1 = \text{work}$) altera proporcionalmente a execução paralela;
        \item O período ($T_\infty = \text{span}$), ou tempo para percorrer o caminho crítico, impacta a escalabilidade, ainda que $P$ seja finito.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Folga no paralelismo}
    \begin{itemize}
        \item \alert{Speedup linear}: 
        \[
            T_P \approx \frac{T_1}{P}
        \]
        \item \alert{Folga paralela}: 
        \[
            \frac{T_1}{T_\infty} > > P
        \]
        \item Paralelismo suficiente implica speedup linear:
        \[
             T_P \approx \frac{T_1}{P} \text{ se } \frac{T_1}{T_\infty} > > P
        \]
    \end{itemize}
\end{frame}

\section{Escalabilidade em execução paralela}

\begin{frame}
    \frametitle{Por que as aplicações não escalam?}
    \begin{itemize}
        \item Performance sequencial;
        \item Caminho crítico: as dependências entre as computações se espalham entre os processadores;
        \item Gargalos: um processador pode estar segurando recursos;
        \item Sobrecarga de algoritmos: algumas tarefas podem ser mais custosas para executar em paralelo;
        \item Sobrecarga na comunicação: gastar um tempo cada vez maior em comunicação;
        \item Desbalanceamento de carga:
        \begin{itemize}
            \item Todos os processadores esperam pelo mais lento;
            \item Comportamento dinâmico;
        \end{itemize}
        \item Perda especulativa: faça A e B em paralelo, mas B não é realmente necessário.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Caminho crítico}
    \begin{itemize}
        \item Cadeia de dependências grande demais:
        \begin{itemize}
            \item Maior limitação de desempenho;
            \item Resistente a melhorias.
        \end{itemize}
        \item Diagnóstico:
        \begin{itemize}
            \item O desempenho fica estagnado em um valor relativamente fixo;
            \item Análise do caminho crítico.
        \end{itemize}
        \item Solução:
        \begin{itemize}
            \item Elimine grandes cadeias de dependência sempre que possível;
            \item Diminua as cadeias removendo trabalho do caminho crítico.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Gargalos}
    \begin{itemize}
        \item Como detectar?
        \begin{itemize}
            \item O processador A está ocupado enquanto outros esperam;
            \item Dependência de dados nos resultados produzidos por A.
        \end{itemize}
        \item Situações típicas:
        \begin{itemize}
            \item Redução N para 1 / computação / propagação e 1 para N;
            \item Um processador atribuindo tarefas em resposta a requisições.
        \end{itemize}
        \item Técnicas de solução:
        \begin{itemize}
            \item Comunicação mais eficiente;
            \item Esquemas hierárquicos do tipo master-slave.
        \end{itemize}
        \item O programa pode não apresentar comportamento inadequado por muito tempo;
        \item Contudo, ao escalar os problemas aparecem.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Sobrecarga de algoritmos}
    \begin{itemize}
        \item Muitos algoritmos sequenciais diferentes para resolver o mesmo problema;
        \item Todos os algoritmos paralelos são sequenciais quando rodam em um processador;
        \item Todos os algoritmos paralelos introduzem operações adicionais: \alert{sobrecarga de paralelismo};
        \item Onde deve começar um algoritmo paralelo?
        \begin{itemize}
            \item O melhor algoritmo sequencial pode não paralelizar tudo;
            \item Pode ainda não paralelizar bem, ou seja, não escalar.
        \end{itemize}
        \item O que fazer?
        \begin{itemize}
            \item Escolha variações de algoritmos para minimizar a sobrecarga;
            \item Utilize algoritmos de dois níveis.
        \end{itemize}
        \item Desempenho é a questão:
        \begin{itemize}
            \item A performance paralela está melhorando?
            \item É necessário comparar com o melhor algoritmo sequencial.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Qual o melhor nível de paralelismo possível?}
    \begin{itemize}
        \item Depende da aplicação, algoritmo, programa;
        \item Dependência de dados para a execução;
        \item Metodologia $MaxPar$:
        \begin{itemize}
            \item Analisa o tempo mais curto em que qualquer dado pode ser computado;
            \item Assume um modelo simples para o tempo que a instrução leva para ser executada ou carregar na memória;
            \item O resultado é o maior paralelismo disponível.
        \end{itemize}
        \item O paralelismo \alert{varia}!
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Computação de paralelismo embaraçoso}
    \begin{itemize}
        \item Nenhuma ou pouca comunicação entre os processos;
        \item Cada processo pode executar suas tarefas sem se comunicar com os outros;
        \item Exemplos:
        \begin{itemize}
            \item Integração numérica;
            \item Álgebra linear;
            \item Métodos de Monte Carlo.
        \end{itemize}
    \end{itemize}
    \bigskip
    \begin{figure}[ht!]
         \centering
         \includegraphics[width=.7\textwidth]{../imagens/paralelismo-embaracoso}
         \label{fig:paralelismo-embaracoso}
         \caption{Exemplo de paralelismo embaraçoso \cite{oregoncis410}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Técnicas analíticas / teóricas}
    \begin{itemize}
        \item Fórmulas algébricas e taxas simples:
        \begin{itemize}
            \item Variáveis típicas: tamanho dos dados ($N$), número de processadores ($p$), constantes de máquina;
            \item O objetivo é medir o desempenho de operações individuais, componentes e algoritmos utilizando as variáveis existentes.
            \begin{itemize}
                \item É preciso ter cuidado ao caracterizar variações entre os processadores;
                \item Modelar sempre com operadores máximos.
            \end{itemize}
            \item A utilização de constantes é importante na prática: análise assintótica (deve ser utilizada com cuidado);
        \end{itemize}
        \item Análise da escalabilidade: \alert{isoeficiência} ou \alert{coeficiente de Kumar}.
    \end{itemize}
\end{frame}

\section{Isoeficiência}

\begin{frame}
    \frametitle{Isoeficiência}
    \begin{itemize}
        \item O objetivo é quantificar a escalabilidade;
        \item Qual o aumento do tamanho do problema que é necessário para manter a mesma eficiência em um computador maior?
        \begin{itemize}
            \item Eficiência:
            \begin{itemize}
                \item $T_1/(p \times T_p)$;
                \item $T_p$ = computação + comunicação + espera;
            \end{itemize}
        \end{itemize}
    \end{itemize}
    \begin{figure}[ht!]
        \begin{minipage}[ht]{0.69\textwidth}
	        \begin{itemize}
	            \item Isoeficiência
	            \begin{itemize}
		            \item Equação para curvas de igual eficiência;
		            \item Se não houver solução, o problema \alert{não é escalável} como definido pela isoeficiência.	            
	            \end{itemize}
	        \end{itemize}
	    \end{minipage}
	    \begin{minipage}[ht]{0.29\textwidth}
	        \includegraphics[width=1\textwidth]{../imagens/efficiency-curves}
	    \end{minipage}
	    \label{fig:efficiency-curves}
        \caption{Isoeficiência de Kumar \cite{oregoncis410}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo: adicionando $n$ números ao problema}
    \begin{itemize}
        \item A escalabilidade de um sistema paralelo é a capacidade que ele tem de aumentar o speedup com a adição de novos processadores;
    \end{itemize}
    \begin{figure}
        \begin{minipage}[ht]{0.49\textwidth}
	        \begin{itemize}
	            \item Adicionando $n$ números em $p$ processos com particionamento em faixas:
	            \[
	                \begin{split}
	                    T_{par} &= \frac{n}{p} -1 +2\log(p) \\
	                    S_p &= \frac{n-1}{\frac{n}{p} -1 +2\log(p)} \\
	                     &\approx \frac{n-1}{\frac{n}{p} +2\log(p)} \\
	                    E_p &= \frac{S_p}{p} = \frac{n}{n + 2p\log(p)}
	                \end{split}
	            \]
	        \end{itemize}
	    \end{minipage}
	    \begin{minipage}[ht]{0.49\textwidth}
	        \includegraphics[width=.9\textwidth]{../imagens/scalability-numbers}
	    \end{minipage}
	    \label{fig:scalability-numbers}
	    \caption{Exemplo de análise de escalabilidade \cite{oregoncis410}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Tamanho do problema e sobrecarga}
    \begin{itemize}
        \item Informalmente, o tamanho de um problema é expresso como um parâmetro dos dados de entrada;
        \item Uma definição consistente do tamanho do problema é o número total de operações básicas ($T_{seq}$);
        \item Também é possível se referir ao tamanho do problema como processamento ou \emph{work} ($W = T_{seq}$);
        \item A sobrecarga em um sistema paralelo é definida como a parte do custo que não está no melhor algoritmo serial;
        \item Definida como $T_O$, é uma função de $W$ e $p$:
        \[
            \begin{split}
                T_O(W, p) &= pT_{par} - W \text{ (o trecho } pT_{par} \text{ inclui a sobrecarga)} \\
                T_O(W, p) + W &= pT_{par}
            \end{split}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Função de isoeficiência}
    \begin{itemize}
        \item Se a eficiência for constante, $W = T_{seq}$ é uma função de $p$
    \end{itemize}
    \[
        \begin{split}
            T_{par} &= \frac{W + T_O(W, p)}{p} \\
            S_p &= \frac{W}{T_{par}} = \frac{W_p}{W + T_O(W, p)} \\
            E_p &= \frac{S}{p} = \frac{W}{W + T_O(W, p)} = \frac{1}{1 + \frac{T_O(W, p)}{W}} \\
            E_p &= \frac{1}{1 + \frac{T_O(W, p)}{W}} \Rightarrow \frac{T_O(W, p)}{W} = \frac{1 - E_p}{E_p}
        \end{split}
    \]
    \begin{itemize}
        \item Assim, é possível definir a \alert{função de isoeficiência}:
    \end{itemize}
    \begin{equation}
        W = \frac{E}{1-E}T_O(W, p) = KT_O(W, p)
        \label{eq:kumar}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Isoeficiência no problema de adicionar $n$ números}
    \begin{itemize}
        \item Função de sobrecarga: $T_O(W, p) = pT_{par} - W = 2p\log(p)$
        \item Função de isoeficiência: $W = k * 2p\log(p)$
        \item Se $p$ dobra, $W$ também precisa dobrar para manter aproximadamente a mesma eficiência;
        \item Funções de isoeficiência podem ser mais difíceis de expressar para algoritmos mais complexos.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Outras funções de isoeficiência}
    \begin{itemize}
        \item Uma típica função de sobrecarga $T_O$ pode ter muitos termos distintos com diferentes ordens de magnitude no que diz respeito a $p$ e $W$;
        \item É possível balancear $W$ em cada termo de $T_O$ e computar as respectivas funções de isoeficiência para os termos individuais:
        \begin{itemize}
            \item Mantenha somente o termo que requer a maior taxa de crescimento com relação a $p$[]
            \item Isoeficiência assintótica.
        \end{itemize}
        \item Modelo de complexidade PRAM;
        \item Modelo BSP;
        \item Modelo LogP
        \item Outros modelos mais modernos (computação quântica).
    \end{itemize}
\end{frame}




\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../computacao-paralela}
\bibliographystyle{apalike}

\end{document}
